setopt autolist
unsetopt menucomplete

setopt inc_append_history
setopt share_history

bindkey -v

export KEYTIMEOUT=1

HISTSIZE=SAVEHIST=100000
HISTFILE=~/.zsh_history

[[ -n "${key[PageUp]}"   ]]  && bindkey  "${key[PageUp]}"    history-beginning-search-backward
[[ -n "${key[PageDown]}" ]]  && bindkey  "${key[PageDown]}"  history-beginning-search-forward
bindkey -v '^?' backward-delete-char

vim3() {
    PYTHONPATH=$VIRTUAL_ENV/lib/python3.4/site-packages/:$PYTHONPATH vim $@
}

alias ls='ls --color=auto --group-directories-first'
alias la='ls -lah'
alias lah='ls -lAFh'
alias grep='grep --color=auto -i'
alias can-has='sudo apt-get install'
alias pyongyang="ping"
alias typecheck="ghc-mod check"
alias sprunge="curl -F 'sprunge=<-' http://sprunge.us"
alias last="PAGER=less git lg"
alias xm='setxkbmap gb; xmodmap -e "clear Lock" && xmodmap -e "keysym Caps_Lock = Escape"'
alias suod="sudo"
alias mouse_setup="sudo ~/Programming/C/logitech-mx-performance-dpi/performance_mx_dpi 16"
alias co="source ~/.colours.sh"
alias run-sen='docker run --rm -it -v /var/run/docker.sock:/run/docker.sock --privileged -e TERM tomastomecek/sen'
alias di='docker images | tail -n +2 | sort'
alias cm='compose-mode'
alias cl='compose-deploy'
alias cr='compose-deploy remote'
alias gu='git push && git push --tags'
alias gy='git pull'
alias gb='git branch -vv'
alias gba='git branch -avv'
alias go='git checkout'
alias gs='git status'
alias gub='git push -u origin $(git rev-parse --abbrev-ref HEAD)'
alias gl='git log'
alias gom='git checkout master'
alias http='http --follow'
alias get='http --follow GET'
alias post='http --follow POST'
alias put='http --follow PUT'
alias urldecode='python2 -c "import sys, urllib as ul; print ul.unquote_plus(sys.stdin.read())"'
alias memo='vim ~/memo/`date +%Y-%m-%d`.memo'
alias yesterday='vim ~/memo/`date -d "yesterday 13:00" +%Y-%m-%d`.memo'

export NLTK_DATA=/usr/share/nltk/data/
export EDITOR=vim
export PATH=~/.pyenv/bin:~/Android/Sdk/platform-tools:$PATH:~/.bin:~/.local/bin

source ~/.colours.sh
source ~/.local/bin/virtualenvwrapper.sh
source ~/.zsh/ssh-agent.zsh

if [ -f `which powerline-daemon` ]; then
    powerline-daemon -q
fi

if [[ -r ~/.local/lib/python2.7/site-packages/powerline/bindings/zsh/powerline.zsh ]]; then
    source ~/.local/lib/python2.7/site-packages/powerline/bindings/zsh/powerline.zsh
fi
