\.sh$
#!/bin/bash
# {dirname}/{basename}:
# 
set -eu
set -o pipefail

err() {{
    echo "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: $@" >&2
}}

